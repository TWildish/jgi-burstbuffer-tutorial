#!/bin/bash

for f in bb*.conf
do
  echo "Submit job for $f"
  j=`echo $f | sed -e's%^bbf-%%' -e 's%.conf$%%'`
  sbatch --job-name=$j --bbf="$f" --output=slurm.$j.out --error=slurm.$j.err test-bbf-slurm.sh
done
