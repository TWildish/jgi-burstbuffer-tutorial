#!/bin/bash

set -ex
cd $BSCRATCH
dd if=/dev/urandom of=a bs=1024k count=10
cat a a a a a a a a a a > b
cat b b b b b b b b b b > c
cat c c c c c c c c c c > d
cat d d d d > very-big-file.junk
rm a b c d
