#!/bin/bash
#SBATCH --qos=jgi
#SBATCH --constraint haswell
#SBATCH --time 00:20:00
#SBATCH --job-name TW-use
#SBATCH --output slurm-persistent-use.out
#SBATCH --error  slurm-persistent-use.err
#DW persistentdw name=TW_BB

echo "Using persistent reservation"
cd $DW_PERSISTENT_STRIPED_TW_BB


dd if=/dev/urandom of=a bs=1024k count=10
cat a a a a a a a a a a > b
cat b b b b b b b b b b > c
cat c c c c c c c c c c > d
cat d d d d > very-big-file.junk
rm a b c d
ls -lh

echo ' '
echo "Dumping relevant environment variables"
echo "SLURM Job ID = $SLURM_JOBID"
echo "Slurm Job Name = $SLURM_JOB_NAME"

echo ' '
env | egrep '^DW_' | sort
for v in `env | egrep '^DW_' | awk -F= '{ print $1 }'`
do
  echo ' '
  df -h ${!v}
  find ${!v} -type f -ls
done

echo ' '
echo "Dumping dwstat info"
module load dws
sessID=$(dwstat sessions | grep $SLURM_JOBID | awk '{print $1}')
echo "session ID is: "${sessID}
instID=$(dwstat instances | grep $sessID | awk '{print $1}')
echo "instance ID is: "${instID}
echo "fragments list:"
echo "frag state instID capacity gran node"
