# README #

This repository holds material for the JGI Cori + BurstBuffer training, given in June 2017

For more details about the BurstBuffer, see [https://www.nersc.gov/users/computational-systems/cori/burst-buffer/][https://www.nersc.gov/users/computational-systems/cori/burst-buffer/]
