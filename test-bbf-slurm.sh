#!/bin/bash
#SBATCH --qos=jgi
#SBATCH --time=00:10:00
#SBATCH -N 1
#SBATCH --constraint haswell

echo "Dumping relevant environment variables"
echo "SLURM Job ID = $SLURM_JOBID"
echo "Slurm Job Name = $SLURM_JOB_NAME"

echo ' '
env | egrep '^DW_' | sort
for v in `env | egrep '^DW_' | awk -F= '{ print $1 }'`
do
  echo ' '
  df -h ${!v}
  find ${!v} -type f -ls
done

echo ' '
echo "Dumping dwstat info"
module load dws
sessID=$(dwstat sessions | grep $SLURM_JOBID | awk '{print $1}')
echo "session ID is: "${sessID}
instID=$(dwstat instances | grep $sessID | awk '{print $1}')
echo "instance ID is: "${instID}
echo "fragments list:"
echo "frag state instID capacity gran node"
dwstat fragments | grep ${instID}
