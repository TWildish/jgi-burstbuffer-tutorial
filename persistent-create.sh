#!/bin/bash
#SBATCH --qos=jgi
#SBATCH --constraint haswell
#SBATCH --time 00:10:00
#SBATCH --job-name TW-create
#SBATCH --output slurm-persistent-create.out
#SBATCH --error  slurm-persistent-create.err
#BB create_persistent name=TW_BB capacity=80GB access=striped type=scratch

echo "Creating persistent reservation"

echo "Dumping relevant environment variables"
echo "SLURM Job ID = $SLURM_JOBID"
echo "Slurm Job Name = $SLURM_JOB_NAME"

echo ' '
env | egrep '^DW_' | sort
for v in `env | egrep '^DW_' | awk -F= '{ print $1 }'`
do
  echo ' '
  df -h ${!v}
  find ${!v} -type f -ls
done

echo ' '
echo "Dumping dwstat info"
module load dws
sessID=$(dwstat sessions | grep $SLURM_JOBID | awk '{print $1}')
echo "session ID is: "${sessID}
instID=$(dwstat instances | grep $sessID | awk '{print $1}')
echo "instance ID is: "${instID}
echo "fragments list:"
echo "frag state instID capacity gran node"
