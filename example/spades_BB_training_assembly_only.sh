#!/usr/bin/env bash
#SBATCH -N 1
#SBATCH -J spades_bb
#SBATCH -t 0:10:00
#SBATCH -p regular
#SBATCH -C haswell
#SBATCH --qos=jgi
#SBATCH --exclusive
#SBATCH -A fungalp
#SBATCH --mem=122G
#DW jobdw capacity=300GB access_mode=striped type=scratch
#SBATCH --image=bioboxes/spades:latest

INPUT=/global/homes/w/wildish/Work/Tutorials/jgi-burstbuffer-tutorial/example/Eco_no3.scac.subsample.fastq
[ -f $INPUT ] || gzip -dv $INPUT.gz

echo "Start: `date`"
MY_TMP=$DW_JOB_STRIPED
shifter spades.py --tmp-dir $MY_TMP -m 115 -o spades_only_assembly_BB --only-assembler  --sc --12 $INPUT
echo "Stop: `date`"
