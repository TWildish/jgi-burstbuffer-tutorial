#!/usr/bin/env bash
#SBATCH -N 1
#SBATCH -J spades_no_bb
#SBATCH -t 0:10:00
#SBATCH -p regular
#SBATCH -C haswell
#SBATCH --qos=jgi
#SBATCH --exclusive
#SBATCH -A fungalp
#SBATCH --mem=122G
#SBATCH --image=bioboxes/spades:latest


INPUT=/global/homes/w/wildish/Work/Tutorials/jgi-burstbuffer-tutorial/example/Eco_no3.scac.subsample.fastq
[ -f $INPUT ] || gzip -dv $INPUT.gz

echo "Start: `date`"
MY_TMP=$BSCRATCH
shifter spades.py --tmp-dir $MY_TMP -m 115 -o spades_only_assembly_BB --only-assembler  --sc --12 $INPUT
echo "Stop: `date`"
